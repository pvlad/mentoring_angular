interface Course {
  id: number;
  title: string;
  creationDate: string;
  duration: number;
  description;
}

interface User {
  id: number;
  firstName: string;
  lastName: string;
}

